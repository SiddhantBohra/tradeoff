#!/bin/bash

#set up variables
image=chatteron/base.chatteron
container=base.chatteron
containerRunFilePath="~/containers/base.chatteron.sh"
noOfVersionToKeep=5

# pull latest code
git fetch --all --tags && git pull

# build the docker image
docker build -t $image:latest .

# remove docker container
docker rm -f $container

# start the container
eval $containerRunFilePath

# remove docker image
docker rmi $image:$1

# tag latest build to the
docker tag $image:latest $image:$1

# get all the images
targets=($(docker images $image -q))

# using the more stable code
# from https://stackoverflow.com/questions/11426529/reading-output-of-a-command-into-an-array-in-bash

j=0;
my_array=()
while IFS= read -r line; do
    if (( j > noOfVersionToKeep )); then
     echo Removing $line;
     docker rmi $line
    fi
    j=$((j+1));
done < <( docker images $image -q );