/* eslint-disable global-require, no-process-env */
import "source-map-support/register";
import "colors";

// use bluebird as default promise
global.Promise = require("bluebird");

import "moment-timezone";
import * as moment from "moment";
// set the default timezone
moment.tz.setDefault(`Asia/Kolkata`);

import "./server/config";
import "./server/index";
