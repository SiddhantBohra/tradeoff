import * as jwt from "jsonwebtoken";
import { VerifyOptions } from "jsonwebtoken";
import config from "../config";
import * as errors from "../errors";
import { NodeUtils } from "./NodeUtils";

const verifyJwtAsync = Promise.promisify<object | string, string, string | Buffer, VerifyOptions>(jwt.verify);

// Reads secret used to sign all tokens
const readTokenSecret = async () => config.get("tokenSecret");

export const JwtUtils = {
  /**
   * Verifies(decodes) a given token with a given secret
   * @param {string} token
   * @return {Promise<object | string>}
   */
  async verifyJWToken(token: string): Promise<object | string> {
    const jwtSecret = await readTokenSecret();
    const verifyOptions = { ignoreExpiration: true };
    try {
      // verify the token for signature authenticity
      return await verifyJwtAsync(token, jwtSecret, verifyOptions);
    } catch (error) {
      // decorate the error while decoding
      switch (error.name) {
        // token expiration error
        case `TokenExpiredError`:
          throw new errors.UnauthorizedError({ message: error.message });

        // token malformed, signature missing, invalid sign etc.
        case `JsonWebTokenError`:
          throw new errors.BadRequestError({ message: error.message });

        default:
          throw error;
      }
    }
  },

  /**
   * Generates a new new token from the given payload
   * @param {string | Buffer | object} payload
   * @param {string} expiryTime
   * @return {Promise<string>}
   */
  async generateToken(payload: string | Buffer | object, expiryTime = `60 days`): Promise<string> {
    const jwtSecret = await readTokenSecret();
    return await new Promise<string>((resolve, reject) => {
      jwt.sign(
        payload,
        jwtSecret,
        {
          expiresIn: expiryTime
        },
        (err, token) => {
          if (err) reject(err);
          resolve(token);
        }
      );
    });
  },

  /**
   * Refresh a token either only one or multiple active at a time
   * @param staleToken
   * @returns {*|{get}}
   */
  async refreshToken(staleToken) {
    const decodedInfo = await JwtUtils.verifyJWToken(staleToken);
    // re-generate token with same info
    return JwtUtils.generateToken(decodedInfo);
  },

  /**
   * Refresh a token either only one or multiple active at a time
   * @param staleToken
   * @returns {*|{get}}
   */
  async getIdFromToken(token: string): Promise<string> {
    const decodedToken: any = await JwtUtils.verifyJWToken(token);
    if (!NodeUtils.isNullOrUndefined(decodedToken._id)) {
      return decodedToken._id;
    } else {
      throw new errors.UnauthorizedError({ message: "Invalid JWT Token" });
    }
  }
};
