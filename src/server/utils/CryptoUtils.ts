import * as crypto from "crypto";
import { HexBase64Latin1Encoding } from "crypto";

// algorithm used to encrypt/decrypt text
const ALGORITHM = `aes256`;

// characters to be used for generating a url friendly random string
const UrlFriendlySymbols = `_-0123456789` + `abcdefghijklmnopqrstuvwxyz` + `ABCDEFGHIJKLMNOPQRSTUVWXYZ`;

/**
 * Hash configuration
 */
interface IHashOptions {
  /**
   * Input text which is to be hashed
   */
  input: string;
  /**
   * Output encoding for the hashed string
   */
  encoding: HexBase64Latin1Encoding;
  /**
   * Algorithm for hashing
   */
  algorithm: string;
}

/**
 * HMAC configuration
 */
interface IHmacOptions {
  /**
   * Input text which is to be HMAC'ed
   */
  input: string;
  /**
   * Output encoding for the signed string
   */
  encoding: HexBase64Latin1Encoding;
  /**
   * Secret used to sign the input
   */
  secret: string;
}

/**
 * Async version of @randomBytes function
 * @param {number} size
 * @return {Promise<Buffer>}
 */
const randomBytesAsync = (size: number): Promise<Buffer> => {
  return new Promise<Buffer>((resolve, reject) => {
    crypto.randomBytes(size, (err, buff) => {
      if (err) return reject(err);
      else return resolve(buff);
    });
  });
};

export const CryptoUtils = {
  /**
   *
   * @param {string} a
   * @param {string} b
   * @return {Promise<boolean>}
   */
  async timeSafeCompare(a: string, b: string) {
    const sa = String(a);
    const sb = String(b);

    const key = await new Promise<Buffer>((resolve, reject) => {
      crypto.pseudoRandomBytes(32, (err, buff) => {
        if (err) return reject(err);
        else return resolve(buff);
      });
    });

    const ah = crypto
      .createHmac(`sha256`, key)
      .update(sa)
      .digest();
    const bh = crypto
      .createHmac(`sha256`, key)
      .update(sb)
      .digest();

    return CryptoUtils.bufferEqual(ah, bh) && a === b;
  },

  /**
   * Generate a cryptographically secure string of the given length & encoding
   * https://gist.github.com/joepie91/7105003c3b26e65efcea63f3db82dfba
   *
   * @param {number} length
   * @param encoding
   * @return {Promise<string>}
   */
  async getRandomString(length: number, encoding = `hex`) {
    const randomBuffer = await randomBytesAsync(length);
    return randomBuffer.toString(encoding);
  },

  /**
   * Generate a cryptographically secure, url friendly string of the given length
   * Inspired from - https://github.com/ai/nanoid
   *
   * @param {number} length
   * @return {Promise<string>}
   */
  async getUrlFriendlyRandomString(length: number) {
    const randomBytes = await randomBytesAsync(length);
    const maxIndex = UrlFriendlySymbols.length - 1;
    let id = ``;

    while (0 < length--) {
      id += UrlFriendlySymbols[randomBytes[length] & maxIndex];
    }
    return id;
  },

  /**
   * Get HMAC using SHA 2512
   * @param hmacOptions
   */
  async createHmac(hmacOptions: IHmacOptions) {
    if (!hmacOptions.input) return hmacOptions.input;

    return crypto
      .createHmac(`sha512`, hmacOptions.secret)
      .update(hmacOptions.input)
      .digest(hmacOptions.encoding);
  },

  /**
   * Get HMAC using SHA 2512
   * @param hashOptions
   */
  createHash(hashOptions: IHashOptions) {
    if (!hashOptions.input) return hashOptions.input;

    return crypto
      .createHash(hashOptions.algorithm)
      .update(hashOptions.input)
      .digest(hashOptions.encoding);
  },

  /**
   * Encode text in hexadecimal string
   * @param textToEncode
   * @return {*}
   */
  encodeHex(textToEncode: string) {
    return Buffer.from(textToEncode, "utf8").toString("hex");
  },

  /**
   * Decodes hex string
   * @param encoded
   */
  decodeHex(encoded: string) {
    return Buffer.from(encoded, "hex").toString("utf8");
  },

  /**
   * Encode in base64
   * @param inputText
   * @return {string}
   */
  encodeBase64(inputText: string) {
    return Buffer.from(inputText, `utf-8`).toString(`base64`);
  },

  /**
   * Decode from base64
   * @param encoded
   * @return {string}
   */
  decodeBase64(encoded: string) {
    return Buffer.from(encoded, `base64`).toString(`utf-8`);
  },

  bufferEqual(a, b) {
    if (a.length !== b.length) {
      return false;
    }
    for (let i = 0; i < a.length; i++) {
      if (a[i] !== b[i]) {
        return false;
      }
    }
    return true;
  }
};
