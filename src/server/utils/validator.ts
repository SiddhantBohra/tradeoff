import * as _ from "lodash";
import * as assert from "assert";
import * as moment from "moment";
import * as validator from "validator";
import { NodeUtils } from "./NodeUtils";

const extendedValidator = { ...validator };

const assertString = input => {
  assert(typeof input === "string", "Validator js validates strings only");
};

extendedValidator.extend = function(name, fn) {
  validator[name] = function() {
    const args = Array.prototype.slice.call(arguments);
    assertString(args[0]);
    return fn.apply(validator, args);
  };
};

extendedValidator.extend(`notContains`, function notContains(str, badString) {
  return !_.includes(str, badString);
});

extendedValidator.extend(`isSlug`, function isSlug(str) {
  return extendedValidator.matches(str, /^[a-z0-9\-_]+$/);
});

extendedValidator.extend(`isTimezone`, function isTimezone(str) {
  return !!moment.tz.zone(str);
});

extendedValidator.extend(`isEmptyString`, function(str) {
  return NodeUtils.isNullOrUndefined(str) || extendedValidator.isEmpty(str);
});

extendedValidator.extend(`isNotEmptyURL`, function isEmptyOrURL(str) {
  if (NodeUtils.isNullOrUndefined(str)) return false;
  if (!_.isString(str)) return false;

  return extendedValidator.isURL(str, {
    require_host: true,
    require_protocol: false
  });
});

export { extendedValidator as validator };
