import * as _ from "lodash";

const TEMPLATE_REGEX = /{{([0-9a-zA-Z~!@#$%^&*()\-_=+;:,<>./?'`\s]+)}}/;

// regex for matching template string anywhere within the text
export const TEMPLATE_CHECK_REGEX = new RegExp(TEMPLATE_REGEX, `i`);

// Regex for matching template as per the new specifications
const NEW_TEMPLATE_REGEX = /{{([0-9a-zA-Z~!$@&_.`\s]+)}}/i;

const defaultOptions = {
  // if absent keys should be modified with null value
  modifyAbsentKeys: false
};

/**
 *
 * @param rawText : String template text to be filled
 * @param filler : Object Used to fill the template
 * @param options : options for the filling
 * @return {*}
 */
export default function(rawText: string, filler: object, options?: typeof defaultOptions) {
  // return when no filler or raw text is given
  if (_.isEmpty(rawText) || _.isEmpty(filler)) return rawText;

  // return invalid input as it is
  if (!_.isString(rawText)) return rawText;

  const compileOptions = options || defaultOptions;
  const modifyAbsentKeys = compileOptions.modifyAbsentKeys;

  const replaceRegex = new RegExp(TEMPLATE_REGEX, `g`);
  return rawText.replace(replaceRegex, function replaceArg(substring, templateKey) {
    const hasTemplateKey = _.has(filler, templateKey);
    if (hasTemplateKey) return _.result(filler, templateKey);

    // leave the template as it is or modify
    return modifyAbsentKeys ? substring : substring;
  });
}
