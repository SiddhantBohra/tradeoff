import * as _ from "lodash";
import * as UriJs from "uri-js";
import * as unidecode from "unidecode";
import * as validator from "validator";
import { NodeUtils } from "./NodeUtils";
import IsURLOptions = ValidatorJS.IsURLOptions;

// regex for punctuation marks
const punctRE = /[\u2000-\u206F\u2E00-\u2E7F\\'!"#$%&()*+,\-.\/:;<=>?@\[\]^_`{|}~]/g;
// regex for space
const spaceRE = /\s+/g;

export class StringUtils {
  /**
   * Convert the text to upper case safely
   * @param {string} text
   * @return {string}
   */
  private static safeToUpperCase = (text: string) => (text ? text.toString().toUpperCase() : ``);

  /**
   * Perform case insensitive equality check between the given strings
   * @param {string} string1
   * @param {string} string2
   * @return {boolean}
   */
  static equals = (string1: string, string2: string) => {
    return StringUtils.safeToUpperCase(string1) === StringUtils.safeToUpperCase(string2);
  };

  /**
   * Perform case insensitive inequality check between the given strings
   * @param {string} string1
   * @param {string} string2
   * @return {boolean}
   */
  static notEquals = (string1: string, string2: string) => {
    return !StringUtils.equals(string1, string2);
  };

  /**
   * Find out substring between the given strings
   * @param {string} str
   * @param {string} beginText
   * @param {string} finishText
   * @return {any}
   */
  static substringBetween = (str: string, beginText: string, finishText: string) => {
    const rightHalf = str.substr(str.indexOf(beginText) + beginText.length);
    if (!rightHalf) return null;
    return rightHalf.substr(0, rightHalf.indexOf(finishText));
  };

  /**
   * Sanitize the passed url
   *
   * 1. Replaces multiple occurrence of slash into one
   * @param {string} url
   * @return {string}
   */
  static sanitizeUrl = (url: string) => {
    return url.replace(/([^:]\/)\/+/g, "$1");
  };

  /**
   * Check if the passed string is empty one
   * @param {string} str
   * @return {boolean}
   */
  static isEmptyString = (str: string) => {
    return NodeUtils.isNullOrUndefined(str) || validator.isEmpty(str);
  };

  /**
   * Check if input can be a valid button text or not
   * @param {string} text
   * @return {boolean}
   */
  static isButtonURL = (text: string): boolean => {
    if (StringUtils.isValidURL(text)) return true;

    // check for other schemes
    try {
      UriJs.parse(text);
      return true;
    } catch (err) {
      return false;
    }
  };

  /**
   * Check if the given url is valid or not.
   *
   * URL string should not be any falsy value
   * @param text
   * @param urlOptions
   * @return {boolean}
   */
  static isValidURL = (text: string, urlOptions?: IsURLOptions) => {
    if (NodeUtils.isNullOrUndefined(text)) return false;
    if (!_.isString(text)) return false;

    const defaultUrlOptions = {
      require_host: true,
      require_protocol: false,
      require_tld: false
    };
    const validationOptions = urlOptions || defaultUrlOptions;
    return validator.isURL(text, validationOptions);
  };

  /**
   * Check if string doesn't contain given string
   * @param str
   * @param text
   * @return {boolean}
   */
  static notContains = (str: string, text: string) => {
    return !_.includes(str, text);
  };

  /**
   * Check if string is a slug
   * @param {string} slug
   * @return {boolean}
   */
  static isSlug = (slug: string) => {
    return validator.matches(slug, /^[a-z0-9\-_]+$/);
  };

  /**
   *
   * @param str
   * @param options
   * @return {any}
   */
  static safeString = (str: string, options: any = {}) => {
    // Handle the £ symbol separately, since it needs to be removed before the unicode conversion.
    let safeText = str.replace(/£/g, `-`);

    // Remove non ascii characters
    safeText = unidecode(safeText);

    // Replace URL reserved chars: `:/?#[]!$&()*+,;=` as well as `\%<>|^~£"`
    safeText = safeText
      .replace(/(\s|\.|@|:|\/|\?|#|\[|]|!|\$|&|\(|\)|\*|\+|,|;|=|\\|%|<|>|\||\^|~|"|–|—)/g, `-`)
      // Remove apostrophes
      .replace(/'/g, "")
      // Make the whole thing lowercase
      .toLowerCase();

    // We do not need to make the following changes when importing data
    if (!_.has(options, `importing`) || !options.importing) {
      // Convert 2 or more dashes into a single dash
      safeText = safeText
        .replace(/-+/g, `-`)
        // Remove trailing dash
        .replace(/-$/, ``)
        // Remove any dashes at the beginning
        .replace(/^-/, ``);
    }

    // Handle whitespace at the beginning or end.
    safeText = safeText.trim();

    return safeText;
  };

  /**
   * Normalize value of a parameter before saving
   * @param attribute
   * @return {any}
   */
  static attributeToString = (attribute: any) => {
    if (NodeUtils.isNullOrUndefined(attribute)) return null;
    if (_.isString(attribute)) return attribute;
    // convert objects to json string
    if (_.isObject(attribute)) return JSON.stringify(attribute);

    return attribute.toString();
  };

  /**
   * Coerce the object to string
   * @param object
   * @return {string}
   */
  static makeString = (object: any) => {
    if (object == null) return ``;
    return `` + object;
  };

  /**
   * Convert string into words separated by underscore
   * @param {string} text
   * @return {string}
   */
  static slugify = (text: string) => {
    return text
      .toString()
      .toLowerCase()
      .replace(/\s+/g, "-") // Replace spaces with -
      .replace(/[^\w\-]+/g, "") // Remove all non-word chars
      .replace(/\-\-+/g, "-") // Replace multiple - with single -
      .replace(/^-+/, "") // Trim - from start of text
      .replace(/-+$/, ""); // Trim - from end of text
  };

  /**
   * Check if the string starts with
   * @param {string} text
   * @param {string} searchString
   * @param {number} position
   * @return {boolean}
   */
  static startsWith = (text: string, searchString: string, position: number = 0) => {
    return text.substr(position, searchString.length) === searchString;
  };

  /**
   * Strip any HTML
   * @param {string} text
   * @return {string}
   */
  static stripTags = (text: string) => {
    return StringUtils.makeString(text).replace(/<\/?[^>]+>/g, ``);
  };

  /**
   * Return sentence array within the given text
   * @param text
   * @return {string[]}
   */
  static getSentences = (text): string[] => {
    return text.replace(/([.?!])\s*(?=([A-Z0-9]|['"“”‘’]))/g, "$1|").split("|");
  };

  /**
   * Remove apostrophe from the passed text
   * @param {string} text
   * @return {string}
   */
  static removeApostrophe = (text: string): string => {
    return text.replace(/'/g, ` `);
  };

  /**
   * Removes punctuations from the given string
   * @param {string} text
   * @return {string}
   */
  static removePunctuations = (text: string): string => {
    return text.replace(punctRE, ``).replace(spaceRE, ` `);
  };

  /**
   * Capitalize the first letter
   * @param {string} text
   * @return {string}
   */
  static capitalizeFirstLetter = (text: string) => {
    return text.charAt(0).toUpperCase() + text.slice(1);
  };

  /* Commenting because ./text/Tokenizer not available in this repo
  /!**
   * return array of string. Each element of the array shall have text which has length with the limit
   * of max-length
   * @param {string} text
   * @param {number} limit
   * @return {any}
   *!/
  static breakText = (text: string, limit: number) => {
    return splitTextIntoChunks(text, limit);
  };
*/

  /**
   * tokenize a sentence into array of words
   * @param {string} text
   * @return {string[]}
   */
  static wordTokenize = (text: string = ""): string[] => {
    let sentence = text || "";

    // most punctuation
    sentence = sentence.replace(/([^\w.'\-\/+<>,&])/g, " $1 ");

    // commas if followed by space
    sentence = sentence.replace(/(,\s)/g, " $1");

    // single quotes if followed by a space
    sentence = sentence.replace(/('\s)/g, " $1");

    // any quote if followed by a space
    // text = text.replace(/('\s)/g, " $1");

    // periods before newline or end of string
    sentence = sentence.replace(/\. *(\n|$)/g, " . ");

    return _.without(sentence.split(/\s+/), "");
  };

  /**
   * Normalize white spaces
   * @param {string} inputText
   * @return {string}
   */
  static normalizeWhiteSpaces = (inputText: string) => {
    return inputText.replace(/\s{2,}/g, ` `);
  };

  /**
   * Clean text of the url
   * @param {string} urlText
   * @return {string}
   */
  static removeWhiteSpaces = (urlText: string) => {
    return urlText.replace(/\s+/g, ``);
  };

  /**
   * Check input text for digits only
   * @param {string} inputText
   * @return {boolean}
   */
  static isDigit = (inputText: string) => {
    if (NodeUtils.isNullOrUndefined(inputText)) return false;
    if (!_.isString(inputText)) return false;

    return /^\d+$/.test(inputText);
  };
}
