import * as _ from "lodash";
import * as qs from "querystring";

export const MongoUtils = {
  /**
   * Creates connection uri for mongo
   * @param dbConfiguration
   * @return {string}
   */
  buildMongoUri(dbConfiguration) {
    const { user, host, schema, password } = dbConfiguration;
    const port = dbConfiguration.port || 27017;

    let mongoUri = `mongodb://`;
    if (user && password) mongoUri += `${encodeURIComponent(user)}:${encodeURIComponent(password)}@`;
    mongoUri += `${host}:${port}/${schema}`;

    const connectionOptions = dbConfiguration.options;
    if (!_.isEmpty(connectionOptions)) mongoUri += `?${qs.stringify(connectionOptions)}`;

    return mongoUri;
  },

  /**
   * Create sparse partial index options for the given field
   * @param {string} prop
   * @return {{partialFilterExpression: {}}}
   */
  sparseIndexOptions(prop: string) {
    return { partialFilterExpression: { [prop]: { $exists: true } } };
  }
};
