import * as requestPromise from "request-promise-native";
import { URL } from "url";
import { validator } from "./validator";
import { StringUtils } from "./StringUtils";
import { ApplicationError } from "../errors";

export interface IWebHookRequestParam {
  uri?: string;
  qs?: object; // query String
  headers?: object;
  body?: object;
  method?: string;
}

export interface IWebHookRequestOptions {
  // params to modify the request behaviour
  followRedirect?: boolean;
  followAllRedirects?: boolean;
  json?: boolean; // this stringifies the requestPromise body and sets the right headers
  encoding?: string;
}

export interface IWebHookRequestParams extends IWebHookRequestParam {
  // param to support the domain and route
  domain?: string;
  route?: string;
}

export class WebHookUtilities {
  /**
   *
   * @param domain
   * @param route
   * @param qs
   * @param headers
   * @param uri
   */
  static getRequestWrapper = async (req: IWebHookRequestParams, options: IWebHookRequestOptions = { json: true }) => {
    req.method = "GET";
    return await WebHookUtilities.requestWrapper(req, options);
  };

  /**
   *
   * @param domain
   * @param route
   * @param qs
   * @param body
   * @param headers
   * @param uri
   */
  static postRequestWrapper = async (req: IWebHookRequestParams, options: IWebHookRequestOptions = { json: true }) => {
    req.method = "POST";
    return await WebHookUtilities.requestWrapper(req, options);
  };

  /**
   *
   * @param domain
   * @param route
   * @param qs
   * @param body
   * @param headers
   * @param uri
   */
  static requestWrapper = async (
    req: IWebHookRequestParams = {
      domain: "",
      route: "",
      qs: {},
      headers: {},
      uri: ""
    },
    options: IWebHookRequestOptions = {
      json: true
    }
  ) => {
    try {
      req.uri = StringUtils.isEmptyString(req.uri) ? new URL(req.route, req.domain).href : req.uri;

      // skip invalid url hits
      if (!validator.isURL(req.uri)) {
        console.log(`Invalid URL : ${req.uri}`);
        throw new ApplicationError({ message: `Invalid URL : ${req.uri}` });
      }

      return await requestPromise({ ...req, ...options } as any);
    } catch (error) {
      // in try catch because requestPromise send a humongous reply on throw message
      if (error.statusCode) {
        // debug.error(error.statusCode);
        throw new ApplicationError({ message: `HTTP error ::  ${error.statusCode}, while connecting to ${req.uri}, message :: ${error.message}` });
      }
      throw new ApplicationError(error);
    }
  };

  static async postDownloadRequestWrapper() {
    try {
      // let res = fs.createReadStream("src/TaiLieuKetNoiDauso8x41_VIHAT_EN.pdf").pipe(requestPromise.post("http://localhost:3000/api/test1"));
    } catch (error) {
      // in try catch because requestPromise send a humongous reply on throw message
      if (error.statusCode) {
        console.log(error.statusCode);
        error.message = `HTTP error :: ${error.statusCode}`;
      }
      throw new ApplicationError(error);
    }
  }
}
