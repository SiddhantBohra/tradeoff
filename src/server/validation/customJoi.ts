import * as Joi from "joi";
import * as validator from "validator";
import { TEMPLATE_CHECK_REGEX } from "../utils/StringTemplate";

// custom JOI for validation with custom validation methods
export const joi = Joi.extend([
  {
    name: `nonEmptyString`,
    base: Joi.string()
      .min(0)
      .required()
  },
  {
    // Schema for optional string allowing empty string
    name: `optionalString`,
    base: Joi.string().empty(``)
  },
  {
    name: `url`,
    base: Joi.string(),
    language: {
      plain: "Invalid url {{v}}",
      template: "invalid template url : {{v}}"
    },
    pre(value, state, options) {
      return value || "";
    },
    rules: [
      {
        name: `template`,
        params: {
          template: Joi.boolean()
        },
        validate(params, value, state, options) {
          // A valid template url could be empty, a normal url or contain many template texts within
          const isTemplate = params.template || false;

          if (validator.isEmpty(value)) return value;
          if (validator.isURL(value)) return value;

          if (isTemplate) {
            if (TEMPLATE_CHECK_REGEX.test(value)) return value;
            return this.createError("url.template", { v: value }, state, options);
          }

          // Generate an error, state and options need to be passed
          return this.createError("url.plain", { v: value }, state, options);
        }
      }
    ]
  },
  {
    name: `e164`,
    base: Joi.string().regex(/^\+(?:[0-9] ?){6,14}[0-9]$/)
  }
]);
