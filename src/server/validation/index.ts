/**
 * Data for api is validated against a schema.
 *
 * Only validates the keys that are present in the validation schema
 */
import * as _ from "lodash";
import { joi } from "./customJoi";
import * as errors from "../errors";

const defaultOptions = {
  language: {
    any: {
      unknown: "is not allowed",
      invalid: "contains an invalid value",
      empty: "is not allowed to be empty",
      required: "is required",
      default: "threw an error when running default method"
    }
  }
};

const locations = ["body", "headers", "params", "query"];

function throwValidationError(validationErrors) {
  if (validationErrors.length === 0) return;

  // throw the first error
  const errorMsg = _.flatten(_.map(validationErrors, `messages`));

  throw new errors.ValidationError({ message: errorMsg });
}

/**
 * validate checks the current `Request` for validations
 * NOTE: mutates `request` in case the object is valid.
 */
const performValidation = (validationErrors, testData, schema, options) => {
  /** return when no schema to validate against */
  if (!schema) return;

  const joiOptions = _.defaults({}, options);
  const { error, value } = (joi as any).validate(testData, schema, joiOptions);

  if (!error || error.details.length === 0) {
    _.assignIn(testData, value); // joi responses are parsed into JSON
    return;
  }

  for (const errorObj of error.details) {
    const errorExists = _.find(validationErrors, (errorItem: any) => {
      // append the message if the error is in the same field
      if (errorItem && errorItem.field === errorObj.path) {
        errorItem.messages.push(errorObj.message);
        errorItem.types.push(errorObj.type);
        return errorItem;
      }
      return false;
    });

    if (!errorExists) {
      validationErrors.push({
        field: errorObj.path,
        messages: [errorObj.message],
        types: [errorObj.type]
      });
    }
  }
};

export default {
  /**
   * Validate given data against schema
   * @param testData
   * @param schema
   */
  validate(testData: any, schema: any) {
    if (!schema) throw new Error("Please provide a validation schema");

    const validationErrors = [];

    // Set default options
    const options = _.defaults({}, defaultOptions);

    // NOTE: mutates `errors`
    _.forEach(locations, (location: any) => {
      if (schema.hasOwnProperty(location)) {
        performValidation(validationErrors, testData[location], schema[location], options);
      }
    });

    throwValidationError(validationErrors);
  },

  /**
   * Validate data against the given schema
   * @param testData
   * @param schema
   * @param options
   */
  validateBody(testData: any, schema: any, options?: any) {
    const validationErrors = [];
    const localOptions = _.defaults({}, defaultOptions, options, {
      abortEarly: true,
      allowUnknown: true
    });
    performValidation(validationErrors, testData, schema, localOptions);

    throwValidationError(validationErrors);
  }
};

export class ApiValidator {
  /**
   * Validate given data against schema
   * @param testData
   * @param schema
   */
  static validateLocations = (testData: any, schema: any) => {
    if (!schema) throw new Error(`Please provide a validation schema.`);

    const validationErrors = [];

    // Set default options
    const options = _.defaults({}, defaultOptions);

    // NOTE: mutates `errors`
    _.forEach(locations, (location: any) => {
      if (schema.hasOwnProperty(location)) {
        performValidation(validationErrors, testData[location], schema[location], options);
      }
    });

    throwValidationError(validationErrors);
  };

  /**
   * Validate data against the given schema
   * @param testData
   * @param schema
   * @param options
   */
  static validateBody = (testData: any, schema: any, options?: any) => {
    const validationErrors = [];
    const localOptions = _.defaults({}, defaultOptions, options, {
      abortEarly: true,
      allowUnknown: true
    });

    performValidation(validationErrors, testData, schema, localOptions);

    throwValidationError(validationErrors);
  };
}
