// # Main Server
// Handles the creation of an HTTP Server for
import * as express from "express";
import * as http from "http";
import * as moment from "moment";
import config from "./config";
import { GlobalEventRegistry } from "./events";

const debug = require("debug")("tradeoff:server");

/**
 * ## tradeOff Server
 * @constructor
 * @param {Object} rootApp - parent express instance
 */
export class HttpServer {
  public httpServer: http.Server = null;
  public connections: any;
  public connectionId: number;
  public config: any;

  constructor(public rootApp) {
    this.rootApp = rootApp;
    this.connections = {};
    this.connectionId = 0;

    // Expose config module for use externally.
    this.config = config;
  }

  /**
   * ## Public API methods
   *
   * ### Start
   * Starts server listening on the configured port.
   * Alternatively you can pass in your own express instance and let Ghost
   * start listening for you.
   * @param  {Object} externalApp - Optional express app instance.
   * @return {Promise} Resolves once Ghost has started
   */
  start = async (externalApp?: express.Application) => {
    debug(`Starting...`);
    const rootApp = externalApp ? externalApp : this.rootApp;

    await new Promise((resolve, reject) => {
      const port = config.get(`server`).port;
      const host = config.get(`server`).host;

      this.httpServer = rootApp.listen(port, host);

      this.httpServer.on(`error`, (error: Error) => {
        let serverError: Error;
        const errorNo = error[`errno`];
        if (errorNo === `EADDRINUSE`) {
          serverError = new Error(
            `(EADDRINUSE) Cannot start Server. Port  ${port} ` + `is already in use by another program. Is another Server instance already running?`
          );
        } else {
          serverError = new Error(
            `(Code: ' ${errorNo} ') There was an error starting your server.` + `Please use the error code above to search for a solution.`
          );
        }
        reject(serverError);
      });

      this.httpServer.on(`connection`, this.connection);

      this.httpServer.on(`listening`, () => {
        debug(`...Started`);
        this.logStartMessages();
        resolve(this);
      });
    });

    GlobalEventRegistry.emit(`server.started`, this.httpServer, rootApp);
  };

  /**
   * ### Stop
   * Returns a promise that will be fulfilled when the server stops. If the server has not been started,
   * the promise will be fulfilled immediately
   * @returns {Promise} Resolves once Ghost has stopped
   */
  stop() {
    return new Promise(resolve => {
      if (this.httpServer === null) {
        resolve(this);
      } else {
        this.httpServer.close(() => {
          this.httpServer = null;
          this.logShutdownMessages();
          resolve(this);
        });
        this.closeConnections();
      }
    });
  }

  /**
   * ### Restart
   * Restarts the application
   * @returns {Promise} Resolves once application has restarted
   */
  restart = () => {
    return this.stop().then(this.start);
  };

  /**
   * ## Private (internal) methods
   *
   * ### Connection
   * @param {Object} socket
   */
  private connection = socket => {
    this.connectionId += 1;
    socket._connId = this.connectionId;

    socket.on("close", () => {
      delete this.connections[socket._connId];
    });

    this.connections[socket._connId] = socket;
  };

  /**
   * ### Close Connections
   * Most browsers keep a persistent connection open to the server, which prevents the close callback of
   * httpServer from returning. We need to destroy all connections manually.
   */
  closeConnections = () => {
    Object.keys(this.connections).forEach(socketId => {
      const socket = this.connections[socketId];

      // kill the socket connection
      if (socket) {
        socket.destroy();
      }
    });
  };

  /**
   * ### Log Start Messages
   */
  private logStartMessages = () => {
    // Startup & Shutdown messages
    if (process.env.NODE_ENV === `production`) {
      console.log(
        `Server is running in ${process.env.NODE_ENV} ...`.green,
        `\nIt's live on`,
        config.get(`server:url`),
        `\nListening on`,
        config.get(`server`).host + ":" + config.get(`server`).port,
        `\nCtrl+C to shut down`.grey
      );
    } else {
      console.log(
        `Server is running in ${process.env.NODE_ENV} ...`.green,
        `\nListening on`,
        config.get(`server`).host + `:` + config.get(`server`).port,
        `\nUrl configured as:`,
        config.get(`server:url`),
        `\nCtrl+C to shut down`.grey
      );
    }

    async function shutdown() {
      // clean-up the resources
      GlobalEventRegistry.emit(`server.shutdown`);

      console.log(`\nServer is shutting down.`.red);
      if (process.env.NODE_ENV !== `production`) {
        console.log(`\nServer was running for`, moment.duration(process.uptime(), `seconds`).humanize());
      }
      setTimeout(() => process.exit(0), 400);
    }

    // ensure that Ghost exits correctly on Ctrl+C and SIGTERM
    process
      .removeAllListeners(`SIGINT`)
      .on(`SIGINT`, shutdown)
      .removeAllListeners(`SIGTERM`)
      .on(`SIGTERM`, shutdown);
  };

  /**
   * ### Log Shutdown Messages
   */
  private logShutdownMessages = () => {
    console.log(`Server is closing connections`.red);
  };
}
