// # API routes
import { tmpdir } from "os";
import * as express from "express";
import * as bodyParser from "body-parser";

import api from "../api";
import cors from "../middleware/api/cors";
import maintenance from "../middleware/maintenance";
import cacheControl from "../middleware/cache-control";
import errorHandler from "../middleware/error-handler";

import { RateLimiter } from "./utils/RateLimit";

const upload = require("multer")({ dest: tmpdir() });
const debug = require("debug")("tradeoff:api");

// middleware for authenticate routes
const authenticateUser = [
  // auth.authenticate.authenticateJwtUser,
  RateLimiter.publicRates()
];

// middleware for login/signup routes
const signupMiddleware = [RateLimiter.limitSignInRates()];

// @TODO refactor/clean this up - how do we want the routing to work long term?
const apiRoutes = async () => {
  const apiRouter: express.Router = express.Router();

  // ## CORS pre-flight check
  apiRouter.options(`*`, cors);

  // enable cors
  apiRouter.use(cors);

  apiRouter.get("/hello", api.http(api.testApi.helloWorld));

  return apiRouter;
};

module.exports = async () => {
  debug(`API setup start`);
  const apiApp = express.Router();

  // API middleware
  // parse application/json
  apiApp.use(bodyParser.json({ limit: `3mb` }));

  // parse application/x-www-form-urlencoded
  apiApp.use(bodyParser.urlencoded({ extended: true, limit: `5mb` }));

  // send 503 json response in case of maintenance
  apiApp.use(maintenance);

  // API shouldn't be cached
  apiApp.use(cacheControl(`private`));

  // Routing
  apiApp.use(await apiRoutes());

  // API error handling
  apiApp.use(errorHandler.resourceNotFound);
  apiApp.use(errorHandler.handleJSONResponse);

  debug(`API setup end`);

  return apiApp;
};
