import * as url from "url";
import * as _ from "lodash";

import { testApi } from "./TestApi";

let cacheInvalidationHeader;
let locationHeader;
let contentDispositionHeader;

/**
 * ### Init
 * Initialise the API - populate the settings cache
 * @return {Promise(Settings)} Resolves to Settings Collection
 */
const initialize = async function init() {
  return true;
};

/**
 * ### Cache Invalidation Header
 * Calculate the header string for the X-Cache-Invalidate: header.
 * The resulting string instructs any cache in front of the blog that request has occurred which invalidates any cached
 * versions of the listed URIs.
 *
 * `/*` is used to mean the entire cache is invalid
 *
 * @private
 * @param {Express.request} req Original HTTP Request
 * @param {Object} result API method result
 * @return {String} Resolves to header string
 */
cacheInvalidationHeader = function cacheInvalidationHeader(req, result) {
  const parsedUrl = req._parsedUrl.pathname.replace(/^\/|\/$/g, "").split("/");
  const method = req.method;
  const endpoint = parsedUrl[0];
  const jsonResult = result.toJSON ? result.toJSON() : result;

  let cacheInvalidate;
  let post;
  let hasStatusChanged;
  let wasDeleted;
  let wasPublishedUpdated;

  if (method === "POST" || method === "PUT" || method === "DELETE") {
    if (endpoint === "settings" || endpoint === "users" || endpoint === "db" || endpoint === "tags") {
      cacheInvalidate = "/*";
    } else if (endpoint === "posts") {
      post = jsonResult.posts[0];
      hasStatusChanged = post.statusChanged;
      wasDeleted = method === "DELETE";
      // Invalidate cache when post was updated but not when post is draft
      wasPublishedUpdated = method === "PUT" && post.status === "published";

      // Remove the statusChanged value from the response
      delete post.statusChanged;

      // Don't set x-cache-invalidate header for drafts
      if (hasStatusChanged || wasDeleted || wasPublishedUpdated) {
        cacheInvalidate = "/*";
      }
    }
  }

  return cacheInvalidate;
};

/**
 * ### Location Header
 *
 * If the API request results in the creation of a new object, construct a Location: header which points to the new
 * resource.
 *
 * @private
 * @param {Express.request} req Original HTTP Request
 * @param {Object} result API method result
 * @return {String} Resolves to header string
 */
locationHeader = function locationHeader(req, result) {
  let apiRoot;
  let location;
  let newObject;

  if (req.method === "POST") {
    // if (result.hasOwnProperty('posts')) {
    //    newObject = result.posts[0];
    //    location  = apiRoot + '/posts/' + newObject.id + '/?status=' + newObject.status;
    // } else if (result.hasOwnProperty('notifications')) {
    //    newObject = result.notifications[0];
    //    location  = apiRoot + '/notifications/' + newObject.id + '/';
    // } else if (result.hasOwnProperty('users')) {
    //    newObject = result.users[0];
    //    location  = apiRoot + '/users/' + newObject.id + '/';
    // } else if (result.hasOwnProperty('tags')) {
    //    newObject = result.tags[0];
    //    location  = apiRoot + '/tags/' + newObject.id + '/';
    // }
  }

  return location;
};

/**
 * ### Content Disposition Header
 * create a header that invokes the 'Save As' dialog in the browser when exporting the database to file. The 'filename'
 * parameter is governed by [RFC6266](http://tools.ietf.org/html/rfc6266#section-4.3).
 *
 * For encoding whitespace and non-ISO-8859-1 characters, you MUST use the "filename*=" attribute, NOT "filename=".
 * Ideally, both. Examples: http://tools.ietf.org/html/rfc6266#section-5
 *
 * We'll use ISO-8859-1 characters here to keep it simple.
 *
 * @private
 * @see http://tools.ietf.org/html/rfc598
 * @return {string}
 */
contentDispositionHeader = function contentDispositionHeader() {
  // return dataExport.fileName().then(function then(filename) {
  //    return 'Attachment; filename="' + filename + '"';
  // });
};

const addHeaders = function addHeaders(apiMethod, req, res, result) {
  let cacheInvalidation;
  let location;
  let contentDisposition;

  cacheInvalidation = cacheInvalidationHeader(req, result);
  if (cacheInvalidation) {
    res.set({ "X-Cache-Invalidate": cacheInvalidation });
  }

  res.set("Cache-Control", `no-cache`);

  if (req.method === `POST`) {
    location = locationHeader(req, result);
    if (location) {
      res.set({ Location: location });
      // The location header indicates that a new object was created.
      // In this case the status code should be 201 Created
      res.status(201);
    }
  }

  return contentDisposition;
};

/**
 * ### HTTP
 *
 * Decorator for API functions which are called via an HTTP request. Takes the API method and wraps it so that it gets
 * data from the request and returns a sensible JSON response.
 *
 * @public
 * @param {Function} apiMethod API method to call
 * @return {Function} middleware format function to be called by the route when a matching request is made
 */
const http = function http(apiMethod) {
  return async function apiHandler(req, res, next) {
    // We define 2 properties for using as arguments in API calls:
    const object = req.body;
    const requestKeys = [`file`, `files`, `headers`, `params`, `query`, `clientIp`];
    const requestHeaders = req.headers;
    const httpReferrer = requestHeaders.referrer || requestHeaders.referer || requestHeaders[`client-referrer`];

    const options = {
      ..._.pick(req, requestKeys),
      user: req.user ? req.user : null,
      referrer: httpReferrer && url.parse(httpReferrer).hostname
    };

    try {
      const response = await apiMethod(object, options);
      if (req.method === `DELETE`) {
        // if response isn't empty then send 200
        if (response) return res.status(200).send(response);
        return res.status(204).end();
      }

      const contentType = res.get(`Content-Type`);

      // Keep CSV header and formatting
      if (contentType && contentType.indexOf(`text/csv`) === 0) {
        return res.status(200).send(response);
      }

      if (contentType && contentType.indexOf(`text/xml`) === 0) {
        res.statusCode(200).send(response);
      }

      // CASE: api method response wants to handle the express response
      // example: serve files (stream)
      if (_.isFunction(response)) {
        return response(req, res, next);
      }

      // Send a properly formatting HTTP response containing the data with correct headers
      res.json(response || {});
    } catch (err) {
      // To be handled by the API middleware
      next(err);
    }
  };
};

export default {
  // Extras
  initialize,
  http,
  // api end points
  testApi
};
