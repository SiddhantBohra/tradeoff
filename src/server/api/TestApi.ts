// # Mail API
// API for sending Mail

import { GlobalUtils } from "../utils";
import validation from "../validation";
import { joi } from "../validation/customJoi";
import * as crypto from "crypto";
import { ITransactionId } from "../models/logging/interfaces/ILogDocument";
import { EventLogger, IDebugArg } from "../logging/EventLogger";

const debug = new EventLogger("testApi");
export const testApi = {
  /**
   * Send a single email with the given configuration
   * @param object
   * @param options
   */
  helloWorld: async (object, options) => {
    const transactionId: ITransactionId = {
      nameAPI: "/hello",
      transactionId: crypto.randomBytes(32).toString("hex")
    };
    const debugArg: IDebugArg = {
      methodName: "helloWorld"
    };
    debug.info(transactionId, debugArg, "object :: ", object);
    debug.info(transactionId, debugArg, "options :: ", options);
    validation.validateBody(
      options.query,
      joi.object().keys({
        test: joi.string().required()
      })
    );

    const response = GlobalUtils.responseObject();
    response.message = "Hello World";
    debug.info(transactionId, debugArg, "response :: ", response);
    return response;
  }
};
