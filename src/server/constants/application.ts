/**
 * Created by anand on 14/09/15.
 * File to hold value of constants to be used in the whole application anywhere
 */
import config from "../config";

const serverBaseUrl = config.get("server:url");
const dashboardUrl = config.get("dashboard:url");
const supportEmail = config.get("supportMail");
const documentationUrl = config.get("documentation:url");
const serverUrl = config.get("server").url;
const assetsDirectory = config.get("paths:assets");
const env = process.env.NODE_ENV || "development";

export class AppConstants {
  public static baseUrl = serverBaseUrl;
  public static documentationUrl = documentationUrl;
  public static supportEmail = supportEmail;
  public static dashboardUrl = dashboardUrl;
  public static assetsDirectory = assetsDirectory;

  public static auth = config.get("auth");
  public static env = env;

  // type of email services used in sending email
  static MAIL_SERVICE = {
    GMAIL: "gmail",
    DIRECT: "direct",
    MAILGUN: "mailgun"
  };

  // Global level timezone
  static TIMEZONE = "Asia/Calcutta";
}
