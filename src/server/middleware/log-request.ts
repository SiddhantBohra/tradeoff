import * as uuid from "uuid";
import * as express from "express";
import logging from "../logging";

/**
 * log request information in the standard logger
 * @param req
 * @param res
 * @param next
 */
export default function logRequest(req, res: express.Response, next) {
  const startedAt = process.hrtime();
  const requestId = uuid.v1();

  const logResponse = () => {
    const diff = process.hrtime(startedAt);
    // convert response time to millis
    const diffInMillis = diff[0] * 1e3 + diff[1] * 1e-6;
    const roundedTime = diffInMillis.toFixed(2);
    res[`responseTime`] = `${roundedTime} ms`;
    req.requestId = requestId;
    req.userId = req.user ? req.user._id : null;

    if (req.err) {
      logging.error({ req: req, res: res, err: req.err });
    } else {
      logging.info({ req: req, res: res });
    }

    res.removeListener(`finish`, logResponse);
    res.removeListener(`close`, logResponse);
  };

  res.on(`finish`, logResponse);
  res.on(`close`, logResponse);
  next();
}
