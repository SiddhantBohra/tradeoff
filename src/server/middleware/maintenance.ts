import config from "../config";
import * as errors from "../errors";

export default function(req, res, next) {
  const maintenance = config.get("maintenance:enabled");
  if (!!maintenance) {
    return next(new errors.MaintenanceError({ message: "Server Under Maintenance." }));
  }
  next();
}
