import * as _ from "lodash";
import * as errors from "../errors";
import { ApplicationError, NotFoundError } from "../errors";

const internals = {
  /**
   * Get an error ready to be shown the the user
   *
   * @TODO: support multiple errors
   * @TODO: decouple req.err
   */
  prepareError(err, req, res, next) {
    // In case of a ApplicationError class, use it's data
    // Otherwise try to identify the type of error (mongoose validation, mongodb unique, ...)
    // If we can't identify it, respond with a generic 500 error
    if (!(err instanceof errors.ApplicationError)) {
      // We need a special case for 404 errors
      if (err.statusCode && err.statusCode === 404) {
        err = new NotFoundError({ err: err });
      } else {
        err = new ApplicationError({ err: err, statusCode: err.statusCode });
      }
    }

    if (!err || err.statusCode >= 500) {
      // Try to identify the error...
      // ...
      // Otherwise create an InternalServerError and use it
      // we don't want to leak anything, just a generic error message
      // Use it also in case of identified errors but with httpCode === 500
      err = new ApplicationError({ err: err });
    }

    // Handle validation errors
    // Todo add multiple error messages for validation error
    if (err.name === "ValidationError") {
      // responseErr = new errors.BadRequestError({message: err.message}); // TODO standard message? translate?
      // jsonRes.errors = _.map(err.errors, (mongooseErr) => {
      //     return {
      //         message: mongooseErr.message,
      //         path   : mongooseErr.path,
      //         value  : mongooseErr.value,
      //     };
      // });
    }

    // used for express logging middleware see core/server/app.js
    req.err = err;

    // alternative for res.status();
    res.statusCode = err.statusCode;

    // never cache errors
    res.set({
      "Cache-Control": "no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0"
    });

    next(err);
  },

  JSONErrorRenderer(err, req, res, /*jshint unused:false */ next) {
    // Create json response for the error
    let jsonRes = {} as any;

    /* Attach multiple errors to the response*/
    if (!err.errors) {
      jsonRes.errors = [
        {
          errorType: err.errorType,
          message: err.message
        }
      ];
    } else {
      // todo this case never comes up. fix this
      jsonRes.errors = _.map(err.errors, (errorItem: any) => {
        return {
          errorType: errorItem.errorType,
          message: errorItem.message
        };
      });
    }

    res.json(jsonRes);
  }
};

const errorHandler = {
  resourceNotFound(req, res, next) {
    // TODO, handle unknown resources & methods differently, so that we can also produce
    // 405 Method Not Allowed
    next(new errors.NotFoundError({ message: "Resource not found" }));
  },

  handleJSONResponse: [
    // Make sure the error can be served
    internals.prepareError,
    // Render the error using JSON format
    internals.JSONErrorRenderer
  ],

  handleSyncError(err, req, res, next) {
    internals.prepareError(err, req, res, next);
    internals.JSONErrorRenderer(err, req, res, next);
  }
};

export default errorHandler;
