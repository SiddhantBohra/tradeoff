import * as requestIp from "request-ip";

export default function(req, res, next) {
  req.clientIp = requestIp.getClientIp(req);
  next();
}
