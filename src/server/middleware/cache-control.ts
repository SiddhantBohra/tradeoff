// # CacheControl Middleware
// Usage: cacheControl(profile), where profile is one of 'public' or 'private'
// After: checkIsPrivate
// Before: routes
// App: Admin|API
//
// Allows each app to declare its own default caching rules
import * as _ from "lodash";
import config from "../config";

/**
 * Caching profiles for public and private
 * @type {{public: string; private: string}}
 */
const cacheProfileMap = {
  public: "public, max-age=" + config.get("caching:frontend:maxAge"),
  private: "no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0"
};

export default function cacheControl(cacheProfileKey) {
  let cachingProfile;

  if (_.isString(cacheProfileKey) && cacheProfileMap.hasOwnProperty(cacheProfileKey)) {
    cachingProfile = cacheProfileMap[cacheProfileKey];
  }

  return function cacheControlHeaders(req, res, next) {
    // force private if mentioned in the response
    if (res.isPrivate) res.set({ "Cache-Control": cacheProfileMap.private });
    // apply caching profile
    if (cachingProfile) res.set({ "Cache-Control": cachingProfile });
    next();
  };
}
