import * as _ from "lodash";
import * as fs from "fs-extra";
import * as express from "express";

const debug = require("debug")("tradeOff:reaper");
/**
 *
 * @param err
 * @param filePath
 */
const removeFile = async (err: Error, filePath: string) => {
  debug(`removing file ${filePath}`);
  try {
    const stats = await fs.stat(filePath);
    if (!stats.isFile()) return;

    await fs.unlink(filePath);
  } catch (e) {}
};

/**
 * Auto remove any uploaded files on response end
 * to persist uploaded files, simply move them to a permanent location,
 * or delete the req.files[key] before the response end.
 *
 * @param req
 * @param res
 * @param next
 */
export default async function deleteUploadedFiles(req, res: express.Response, next) {
  const reapFiles = async (err: Error) => {
    // remove listeners to avoid multiple invocations
    res.removeListener(`finish`, reapFiles);
    res.removeListener(`close`, reapFiles);

    let filesToDelete = [];

    // for single file uploads
    req.file && filesToDelete.push(req.file);

    // for multiple file uploads
    if (req.files) {
      const filesArray = _.isArray(req.files) ? req.files : _.reduce(req.files, (result, files) => result.concat(files), []);

      filesToDelete = filesToDelete.concat(filesArray);
    }

    // delete all files
    await Promise.map(filesToDelete, multerFile => {
      return removeFile(err, multerFile.path);
    });
  };

  res.on(`finish`, reapFiles);
  res.on(`close`, reapFiles);

  next();
}
