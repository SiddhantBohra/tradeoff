import * as os from "os";
import * as url from "url";
import * as _ from "lodash";
import * as cors from "cors";
import * as pathToRegex from "path-to-regexp";

import config from "../../config";

const methods = "GET,HEAD,PUT,PATCH,POST,DELETE";

// max age of cors is kept to 4 days for now
const ENABLE_CORS = { origin: true, maxAge: 4 * 86400, methods };
const DISABLE_CORS = { origin: false, methods };

const env = process.env.NODE_ENV || `development`;

// whitelist for allowing cors
const whitelist = [];

// origins which are allowed to make cors
const WHITELIST_ORIGIN = {
  development: [/localhost/, /([a-z0-9]+[.])tradeoff.in/, /[a-z0-9]\.ngrok\.io/],
  staging: [/staging[.]([a-z0-9]+[.])tradeoff.in/, /([-a-zA-Z0-9@:%._\+~#=]{2,256}[.])tradeoff[.]in/, /local.tradeoff.in/],
  production: [/([-a-zA-Z0-9@:%._\+~#=]{2,256}[.])tradeoff[.]in/, /tradeoff.in/]
};

/**
 * Gather a list of local ipv4 addresses
 * @return {Array<String>}
 */
const getIPs = () => {
  let ifaces = os.networkInterfaces(),
    ips = [`localhost`];

  Object.keys(ifaces).forEach(function(ifname) {
    ifaces[ifname].forEach(function(iface) {
      // only support IPv4
      if (iface.family !== `IPv4`) {
        return;
      }

      ips.push(iface.address);
    });
  });

  return ips;
};

const getUrls = () => {
  let urls = [url.parse(config.get(`server:url`)).hostname];

  if (config.get(`urlSSL`)) {
    urls.push(url.parse(config.get(`urlSSL`)).hostname);
  }

  const whitelistedUrls = config.get("cors:whitelist");
  whitelistedUrls.map(whiteUrl => urls.push(whiteUrl));

  return urls;
};

(function initWhitelist() {
  const allowedOrigins = WHITELIST_ORIGIN[env];
  const whiteUrls = [
    // origins that always match: localhost, local IPs, etc.
    ...getIPs(),
    // Trusted urls from config
    ...getUrls(),
    ...allowedOrigins
  ];

  whiteUrls.map(whiteUrl => whitelist.push(new RegExp(whiteUrl)));
})();

// list of routes which are open to all clients
const publicPathRegex = [].map(pathname => pathToRegex(pathname));

/**
 * Check if the request is for a route which is open
 * @param req
 * @return {boolean}
 */
const isOpenRouteRequest = req => {
  const pathname = url.parse(req.url).pathname;
  return _.some(publicPathRegex, pathRegex => pathRegex.test(pathname));
};

/**
 * Checks the origin and enables/disables CORS headers in the response.
 * @param  {Object}   req express request object.
 * @param  {Function} callback  callback that configures CORS.
 * @return {null}
 */
const handleCORS = (req, callback): void => {
  let origin = req.get(`origin`);

  // allow cross origin if not disabled
  if (config.get(`cors:disabled`)) return callback(null, ENABLE_CORS);

  // Request must have an Origin header
  if (!origin) return callback(null, DISABLE_CORS);

  // Origin matches whitelist
  const hostname = url.parse(origin).hostname;
  for (let whiteUrlRegex of whitelist) {
    if (whiteUrlRegex.test(hostname)) return callback(null, ENABLE_CORS);
  }

  // allow requests from all origins for open routes
  if (isOpenRouteRequest(req)) {
    return callback(null, ENABLE_CORS);
  }

  return callback(null, DISABLE_CORS);
};

export default cors(handleCORS);
