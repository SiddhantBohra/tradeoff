import * as path from "path";
import * as fs from "fs-extra";
import * as mongoose from "mongoose";
import { Migrator } from "migrate-mongoose";

import config from "../config";
import { MongoUtils } from "../utils/MongoUtils";

const debug = require("debug")("tradeOff:models");
// use bluebird promise for mongoose promises
(<any>mongoose).Promise = Promise;

let __connectionEstablished = false;

// mongoose migrator instance
let migrator: Migrator;

export const MongoConnector = {
  /**
   * Establish connection to the mongo database
   * @return {Promise<void>}
   */
  async connectToMongo() {
    if (__connectionEstablished) return;
    __connectionEstablished = true;

    const debugMongo = config.get(`database:debug`);
    mongoose.set(`debug`, debugMongo);

    // Connect with the database here
    const dbConfiguration = config.get(`database`);

    const mongoUri = MongoUtils.buildMongoUri(dbConfiguration);

    const timeout = 30 * 1000;

    // recommended options from https://gist.github.com/mongolab-org/9959376
    await mongoose.connect(mongoUri, {
      // Maintain up to `poolSize` socket connections
      poolSize: 8,
      keepAlive: true,
      keepAliveInitialDelay: 40 * 1000,
      connectTimeoutMS: timeout,

      // to fix deprecation warnings
      useNewUrlParser: true,
      useFindAndModify: false,
      useCreateIndex: true
    });

    // pre-load all models
    await fs
      .readdir(__dirname)
      .filter((file: string) => {
        return file.indexOf(`.`) > 0 && file !== `index.js`;
      })
      .map(async fileName => {
        await import(`./${fileName}`);
      });
  },

  /**
   * Close existing mongoose connections.
   * The default mongoose connection is @mongoose.connection
   * When using multiple mongoose connections, we will need to close each connection separately.
   *
   * @return {Promise<void>}
   */
  async closeConnections() {
    await mongoose.connection.close();
  },

  /**
   * Run the migrations defined in the migrations folder
   * @return {Promise<void>}
   */
  async runMigrations() {
    const migrateOnRun = config.get(`database:migrate`);
    if (!migrateOnRun) return;

    debug(`running migrations ...`);
    const migrationFolder = path.resolve(path.join(__dirname, `../../migrations`));
    const migrationCollection = `migrations`;
    const forceTsExtension = process.env.NODE_ENV === `development`;

    migrator = new Migrator(mongoose.connection, migrationCollection, migrationFolder, forceTsExtension);
    // sync migrations from the directory to the database
    await migrator.sync();

    // run all migrations
    await migrator.run(`up`);
  }
};
