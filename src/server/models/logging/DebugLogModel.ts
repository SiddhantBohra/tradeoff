import * as mongoose from "mongoose";
import * as _ from "lodash";
import { ILogMethods, ILogStatics, LogModel } from "./LogModel";
import { StringUtils } from "../../utils/StringUtils";
import { NodeUtils } from "../../utils/NodeUtils";
import { IDebugLogDocument } from "./interfaces/IDebugLogDocument";

export interface IDebugLogMethods extends IDebugLogDocument, ILogMethods {}

export interface IDebugLogStatics extends ILogStatics, mongoose.Model<IDebugLogMethods> {
  testToo: (yo: string) => string;
}

export interface IDebugLogPipe {
  companyName: string;
  webViewName: string;
  language: string;
  logObject: DebugLogModel;
}

const { Schema } = mongoose;

export class DebugLogModel extends LogModel {
  get model() {
    return this.vModel;
  }

  protected vModel: IDebugLogStatics;

  private static pipe: IDebugLogPipe[] = [];

  private constructor(companyName: string, webViewName: string, language: string) {
    super(companyName, webViewName, language);

    this.schema.add({
      data: { type: Schema.Types.Mixed },
      debugType: { type: String, enum: ["info", "verbose", "debug"] }
    });

    this.schema.statics.testToo = async function(string) {
      return string;
    };

    this.vModel = mongoose.model<IDebugLogMethods, IDebugLogStatics>(
      `WebViewHelper_#DebugLog-${companyName}-${webViewName}-${language}`,
      this.schema,
      `WebViewHelper_#DebugLog-${companyName}-${webViewName}-${language}`
    );
  }

  public static getModel(companyName: string, webViewName: string, language: string) {
    const exist: IDebugLogPipe = _.find(DebugLogModel.pipe, (value: any, index) => {
      if (
        StringUtils.equals(companyName, value.companyName) &&
        StringUtils.equals(webViewName, value.webViewName) &&
        StringUtils.equals(language, value.language)
      ) {
        return true;
      }
      return false;
    });

    if (NodeUtils.isNullOrUndefined(exist)) {
      const logObject = new DebugLogModel(companyName, webViewName, language);
      DebugLogModel.pipe.push({
        companyName,
        webViewName,
        language,
        logObject
      });
      return logObject.model;
    }
    return exist.logObject.model;
  }
}
