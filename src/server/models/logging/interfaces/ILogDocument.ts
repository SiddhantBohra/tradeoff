import { BaseModel } from "../../interfaces/BaseModel";

export interface IDebugMeta {
  className: string;
  methodName: string;
}

export interface ITransactionId {
  nameAPI: string;
  transactionId: string;
  associatedTransactionIds?: string[];
}

export interface ILogDocument extends BaseModel {
  typeGuardILogDocument?: "typeGuardILogDocument";
  debugMeta: IDebugMeta;
  transactionId: ITransactionId;
}
