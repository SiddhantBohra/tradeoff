import * as mongoose from "mongoose";
import { baseModel } from "../base";
import { dbDebugMetaSchema, dbTransactionSchema } from "./schema/BaseSchemaLogging";
import { ILogDocument, ITransactionId } from "./interfaces/ILogDocument";

export interface ILogMethods extends mongoose.Document {}

export interface ILogStatics {
  GetLogsByTransactionId: (transactionId: ITransactionId) => Promise<ILogDocument>;
  AddLog: (log: ILogDocument) => Promise<any>;
}

const { Schema } = mongoose;

export class LogModel {
  protected schema = new Schema({});

  protected constructor(companyName: string, webViewName: string, language: string) {
    const model = this.schema.add({
      debugMeta: { type: dbDebugMetaSchema, required: true },
      transactionId: { type: dbTransactionSchema, required: true }
    });

    this.schema.index({ debugMeta: 1 });
    this.schema.index({ meta: 1 });
    this.schema.index({ transactionId: 1 });

    this.schema.statics.GetLogsByTransactionId = async function(transactionId: ITransactionId) {
      const query = {
        transactionId
      };

      // debug.verbose("GetCompanyWebViewConfig :: ",query);
      return await this.findOne(query)
        .lean()
        .exec();
    };

    /**
     *
     * @param log
     * @constructor
     */
    this.schema.statics.AddLog = async function(log: ILogDocument) {
      const save = { ...log, createdAt: new Date(), updatedAt: new Date() };
      return await this.collection.insertOne(save);
    };

    this.schema.plugin(baseModel, {});
  }
}
