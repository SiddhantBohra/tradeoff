import * as mongoose from "mongoose";
import { ILogMethods, ILogStatics, LogModel } from "./LogModel";
import * as _ from "lodash";
import { StringUtils } from "../../utils/StringUtils";
import { NodeUtils } from "../../utils/NodeUtils";
import { IErrorLogDocument } from "./interfaces/IErrorLogDocument";

const { Schema } = mongoose;

export interface IErrorLogMethods extends IErrorLogDocument, ILogMethods {}

export interface IErrorLogStatics extends ILogStatics, mongoose.Model<IErrorLogMethods> {}

export class ErrorLogModel extends LogModel {
  get model() {
    return this.vModel;
  }

  protected vModel: IErrorLogStatics;

  private static pipe: any[] = [];

  private constructor(companyName: string, webViewName: string, language: string) {
    super(companyName, webViewName, language);

    this.schema.add({
      stackTrace: { type: [String] },
      body: { type: Schema.Types.Mixed },
      severity: { type: String, enum: ["critical", "high", "medium", "low", "chill"] }
    });

    this.vModel = mongoose.model<IErrorLogMethods, IErrorLogStatics>(
      `WebViewHelper_#ErrorLog-${companyName}-${webViewName}-${language}`,
      this.schema,
      `WebViewHelper_#ErrorLog-${companyName}-${webViewName}-${language}`
    );
  }

  public static getModel(companyName: string, webViewName: string, language: string) {
    const exist = _.find(ErrorLogModel.pipe, (value: any, index) => {
      if (
        StringUtils.equals(companyName, value.companyName) &&
        StringUtils.equals(webViewName, value.webViewName) &&
        StringUtils.equals(language, value.language)
      ) {
        return true;
      }
      return false;
    });

    if (NodeUtils.isNullOrUndefined(exist)) {
      const logObject = new ErrorLogModel(companyName, webViewName, language);
      ErrorLogModel.pipe.push({
        companyName,
        webViewName,
        language,
        logObject
      });
      return logObject.model;
    }
    return exist.logObject.model;
  }
}
