import { Schema } from "mongoose";

const extend = (input: Schema, definition: any) => {
  const temp = input.clone();
  temp.add(definition);
  return temp;
};

const newSubSchema = (definition: any, id = false) => {
  return new Schema(definition, { _id: id });
};

export const dbRequestMetaSchema = newSubSchema(
  {
    webViewName: { type: Schema.Types.String, required: true },
    company: { type: Schema.Types.String, required: true },
    lang: { type: Schema.Types.String, required: true },
    botID: { type: Schema.Types.String, required: true },
    channelUserID: { type: Schema.Types.String, required: true },
    webViewID: { type: Schema.Types.String, required: true }
  },
  true
);

export const dbTransactionSchema = newSubSchema({
  nameAPI: { type: String, required: true },
  transactionId: { type: String, required: true },
  associatedTransactionIds: { type: [String], required: true }
});

export const dbDebugMetaSchema = newSubSchema({
  className: { type: String, required: true },
  methodName: { type: String, required: true }
});
