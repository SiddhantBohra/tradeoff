import * as _ from "lodash";
import * as uuid from "uuid";

interface IErrorOptions {
  message: string;
}

export class ApplicationError extends Error {
  public statusCode: number;
  public errorType: string;
  public level: string;
  public id: string;
  public context: any;
  public help: any;
  public code: any;
  public errorDetails: any;
  public hideStack: boolean;

  constructor(options) {
    super(options.message);

    options = options || {};
    const self = this;

    if (_.isString(options)) {
      throw new Error("Please instantiate Errors with the option pattern. e.g. new errors.ApplicationError({message: ...})");
    }

    Error.captureStackTrace(this, ApplicationError);

    // assign defaults for the error
    this.statusCode = 500;
    this.errorType = `InternalServerError`;
    this.level = `normal`;
    this.message = `The server has encountered an error.`;
    this.id = uuid.v1();

    // custom overrides
    this.statusCode = options.statusCode || this.statusCode;
    this.level = options.level || this.level;
    this.context = options.context || this.context;
    this.help = options.help || this.help;
    this.errorType = this.name = options.errorType || this.errorType;
    this.errorDetails = options.errorDetails;
    this.code = options.code || null;

    this.message = options.message;
    this.hideStack = options.hideStack;

    // error to inherit from, override!
    // nested objects are getting copied over in one piece (can be changed, but not needed right now)
    // support err as string (it happens that third party libs return a string instead of an error instance)
    if (options.err) {
      if (_.isString(options.err)) {
        options.err = new Error(options.err);
      }

      Object.getOwnPropertyNames(options.err).forEach(function(property) {
        // original message is part of the stack, no need to pick it
        if ([`errorType`, `name`, `statusCode`, ""].indexOf(property) !== -1) {
          return;
        }

        if (property === `stack`) {
          self[property] += "\n\n" + options.err[property];
          return;
        }

        self[property] = options.err[property] || self[property];
      });
    }
  }
}

export class IncorrectUsageError extends ApplicationError {
  constructor(options) {
    super(
      _.merge(
        {
          statusCode: 400,
          level: "critical"
        },
        options
      )
    );
    this.errorType = this.constructor.name;
  }
}

export class NotFoundError extends ApplicationError {
  constructor(options) {
    super(
      _.merge(
        {
          statusCode: 404
        },
        options
      )
    );
    this.errorType = this.constructor.name;
  }
}

export class BadRequestError extends ApplicationError {
  constructor(options) {
    super(
      _.merge(
        {
          statusCode: 400
        },
        options
      )
    );
    this.errorType = this.constructor.name;
  }
}

export class UnauthorizedError extends ApplicationError {
  constructor(options) {
    super(
      _.merge(
        {
          statusCode: 401
        },
        options
      )
    );
    this.errorType = this.constructor.name;
  }
}

export class NoPermissionError extends ApplicationError {
  constructor(options) {
    super(
      _.merge(
        {
          statusCode: 403
        },
        options
      )
    );
    this.errorType = this.constructor.name;
  }
}

export class ValidationError extends ApplicationError {
  constructor(options) {
    super(
      _.merge(
        {
          statusCode: 422
        },
        options
      )
    );
    this.errorType = this.constructor.name;
  }
}

export class UnsupportedMediaTypeError extends ApplicationError {
  constructor(options) {
    super(
      _.merge(
        {
          statusCode: 415
        },
        options
      )
    );
    this.errorType = this.constructor.name;
  }
}

export class VersionMismatchError extends ApplicationError {
  constructor(options) {
    super(
      _.merge(
        {
          statusCode: 400
        },
        options
      )
    );
    this.errorType = this.constructor.name;
  }
}

export class TokenRevocationError extends ApplicationError {
  constructor(options) {
    super(
      _.merge(
        {
          statusCode: 503
        },
        options
      )
    );
    this.errorType = this.constructor.name;
  }
}

export class EmailError extends ApplicationError {
  constructor(options) {
    super(
      _.merge(
        {
          statusCode: 500
        },
        options
      )
    );
    this.errorType = this.constructor.name;
  }
}

export class TooManyRequestsError extends ApplicationError {
  constructor(options) {
    super(
      _.merge(
        {
          statusCode: 429
        },
        options
      )
    );
    this.errorType = this.constructor.name;
  }
}

export class MaintenanceError extends ApplicationError {
  constructor(options) {
    super(
      _.merge(
        {
          statusCode: 503
        },
        options
      )
    );
    this.errorType = this.constructor.name;
  }
}

export class ConflictError extends ApplicationError {
  constructor(options) {
    super(
      _.merge(
        {
          statusCode: 409,
          errorDetails: {}
        },
        options
      )
    );
    this.errorType = this.constructor.name;
  }
}

export class ForbiddenError extends ApplicationError {
  constructor(options) {
    super(
      _.merge(
        {
          statusCode: 403,
          errorDetails: {}
        },
        options
      )
    );
    this.errorType = this.constructor.name;
  }
}
