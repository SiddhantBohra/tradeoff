import * as netjet from "netjet";
import * as helmet from "helmet";
import * as express from "express";
import * as compress from "compression";
import * as bodyParser from "body-parser";
import reapUploads from "./middleware/api/reap-uploads";

import config from "./config";
import logRequest from "./middleware/log-request";
// import {LocalStorage} from "./storage/ContentUploadStorage";

const debug = require("debug")("tradeoff:app");

module.exports = async () => {
  // const uploadManager = new LocalStorage();

  debug("ParentApp setup start");
  const parentApp = express();

  // Make sure 'req.secure' is valid for proxied requests
  // (X-Forwarded-Proto header will be checked, if present)
  parentApp.enable(`trust proxy`);

  // parse application/x-www-form-urlencoded
  parentApp.use(bodyParser.urlencoded({ extended: true, limit: `4mb` }));

  // parse text/xml as plain text
  parentApp.use(bodyParser.text({ type: `text/xml` }));

  // request logging
  parentApp.use(logRequest);

  // delete uploaded files
  parentApp.use(reapUploads);

  // enabled gzip compression
  parentApp.use(compress());

  // Preload link headers
  if (config.get(`preloadHeaders`)) {
    parentApp.use(
      netjet({
        cache: {
          max: config.get(`preloadHeaders`)
        }
      })
    );
  }

  // secure app using headers
  parentApp.use(helmet.noCache());

  // controls browser DNS prefetching
  parentApp.use(helmet.dnsPrefetchControl({ allow: true }));

  // to prevent clickjacking
  parentApp.use(
    helmet.frameguard({
      action: `deny`
    })
  );

  parentApp.use(
    helmet.hidePoweredBy({
      setTo: `PHP 4.2.0`
    })
  );

  // adds some small XSS protections
  parentApp.use(helmet.xssFilter());

  // for HTTP Strict Transport Security
  parentApp.use(
    helmet.hsts({
      includeSubDomains: false
    })
  );

  // sets X-Download-Options for IE8+
  parentApp.use(helmet.ieNoOpen());

  // sets X-Content- Type-Options to keep clients from sniffing the MIME type
  parentApp.use(helmet.noSniff());

  // to manipulate the Referer header
  parentApp.use(helmet.referrerPolicy({ policy: `no-referrer-when-downgrade` }));

  // Sets "X-XSS-Protection: 1; mode=block".
  parentApp.use(helmet.xssFilter());

  parentApp.use(
    helmet.contentSecurityPolicy({
      directives: {
        defaultSrc: ["'self'"],
        scriptSrc: ["'self'", "'unsafe-inline'"],
        styleSrc: ["'self'", "fonts.google.com"]
      }
    })
  );

  // for HTTP Public Key Pinning
  // parentApp.use(helmet.hpkp(false));

  // mount api app
  const apiApp = await require("./api/app")();
  parentApp.use("/api", apiApp);

  const cacheTime = 86400000;
  // const uploadPath = uploadManager.getBaseDirectory();
  // parentApp.use(express.static(uploadPath, {maxAge: cacheTime}));

  debug("ParentApp setup end");

  return parentApp;
};
