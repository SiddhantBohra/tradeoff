import { AppConstants } from "../constants/application";
import { StringUtils } from "../utils/StringUtils";
import * as util from "util";
import { ErrorLogModel } from "../models/logging/ErrorLogModel";
import { IErrorLogDocument, typeSeverity } from "../models/logging/interfaces/IErrorLogDocument";
import { debugType, IDebugLogDocument } from "../models/logging/interfaces/IDebugLogDocument";
import { DebugLogModel } from "../models/logging/DebugLogModel";
import { IDebugMeta, ITransactionId } from "../models/logging/interfaces/ILogDocument";
import { EventEmitter } from "events";
import config from "../config";
import * as _ from "lodash";

const shouldDebugFlow = config.get(`debug`);
const debugEmitter = new EventEmitter();
const env = AppConstants.env;
util.inspect.defaultOptions.colors = true;
util.inspect.defaultOptions.depth = null;

/**
 * Listener for log event, it will make entry in the database
 *  and publish the log over the socket if the debug key is true
 */
debugEmitter.on("log", async (doc: IDebugLogDocument) => {
  const {
    transactionId: { nameAPI }
  } = doc;
  const model = DebugLogModel.getModel("hccb", nameAPI, "en-US");

  // don't await just log
  model.AddLog(doc);

  if (!shouldDebugFlow) {
    return;
  }
});

/**
 * Listener to send mail to all the recipient with Error Log payload
 *
 */
debugEmitter.on("mail", async (doc: IErrorLogDocument) => {
  // style object CSS for prettifying action
  const errorActionStyle = {
    ".json-markup": {
      "padding-left": "30px",
      "padding-right": "30px"
    },
    ".json-markup-selected": {
      "background-color": "rgba(139, 191, 228, 0.19999999999999996)"
    },
    ".json-markup-string": {
      color: "#6caedd"
    },
    ".json-markup-key": {
      color: "#ec5f67"
    },
    ".json-markup-bool": {
      color: "#99c794"
    },
    ".json-markup-number": {
      color: "#99c794"
    }
  };
  /* TODO :: mail logic comes here
    const prettyAction = jsonMarkup(currentAction, ErrorActionStyle);

    // send an email alert for this error
    const content = await MailUtils.generateContent({
      data: {
        module: currentModuleId,
        message: flowError.message,
        action: prettyAction
      },
      template: `flow-error`
    });
    await MailAPI.sendOneEmail(
      {
        mail: {
          from: `info@chatteron.io`,
          to: receiverEmailIds,
          subject: `Flow error in ${botAgentName} - ${botAgentId}`,
          html: content.html,
          text: content.text
        }
      },
      {}
    );*/
});

/**
 *  Listener for error event, it will make entry in the database,
 *  publish the log over the socket and emit event to send mail
 */
debugEmitter.on("error", async (doc: IErrorLogDocument) => {
  const {
    transactionId: { nameAPI }
  } = doc;
  const errorModel = ErrorLogModel.getModel("hccb", nameAPI, "en-US");

  // don't await just log
  errorModel.AddLog(doc);

  // mail it to the recipient
  debugEmitter.emit("mail", doc);
});

export interface IDebugArg {
  methodName: string;
}

export class EventLogger {
  private className: string;
  private fInfo: Function;
  private fVerbose: Function;
  private fDebug: Function;
  private fError: Function;

  constructor(className: string) {
    this.className = className;
    this.fInfo = require("debug")(`info:tradeOff:${className}`);
    this.fVerbose = require("debug")(`verbose:info:tradeOff:${className}`);
    this.fDebug = require("debug")(`debug:verbose:info:tradeOff:${className}`);
    this.fError = require("debug")(`error:debug:verbose:info:tradeOff:${className}`);
  }

  private log(type: Function, doc: IDebugLogDocument) {
    if (!StringUtils.equals(env, "production")) {
      type("%O", doc.data);
    }
    // asyncronously do the logging into the database and socket
    debugEmitter.emit("log", doc);
  }

  public info(transactionId: ITransactionId, debugArg: IDebugArg, ...args: any[]) {
    const debugMeta: IDebugMeta = {
      methodName: debugArg.methodName,
      className: this.className
    };
    const doc: IDebugLogDocument = {
      debugMeta,
      transactionId,
      data: args,
      debugType: debugType.info
    };
    this.log(this.fInfo, doc);
  }

  public verbose(transactionId: ITransactionId, debugArg: IDebugArg, ...args: any[]) {
    const debugMeta: IDebugMeta = {
      methodName: debugArg.methodName,
      className: this.className
    };
    const doc: IDebugLogDocument = {
      debugMeta,
      transactionId,
      data: args,
      debugType: debugType.verbose
    };
    this.log(this.fVerbose, doc);
  }

  public debug(transactionId: ITransactionId, debugArg: IDebugArg, ...args: any[]) {
    const debugMeta: IDebugMeta = {
      methodName: debugArg.methodName,
      className: this.className
    };
    const doc: IDebugLogDocument = {
      debugMeta,
      transactionId,
      data: args,
      debugType: debugType.debug
    };
    this.log(this.fDebug, doc);
  }

  public error(transactionId: ITransactionId, debugArg: IDebugArg, severity: typeSeverity = typeSeverity.critical, stackTrace: string, body: any) {
    const debugMeta: IDebugMeta = {
      methodName: debugArg.methodName,
      className: this.className
    };
    const doc: IErrorLogDocument = {
      debugMeta,
      severity,
      body,
      transactionId,
      stackTrace: _.split(stackTrace, "\n")
    };

    this.fError("%O", doc);
    // asyncronously do the error log into the db, socket and send mail
    debugEmitter.emit("error", doc);
  }
}
