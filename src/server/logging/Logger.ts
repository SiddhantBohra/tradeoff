import * as _ from "lodash";
import * as bunyan from "bunyan";
import { LogLevel, Serializers } from "bunyan";
import { PrettyStream } from "./PrettyStream";
import * as jsonStringifySafe from "json-stringify-safe";

export class DiskLogger {
  private env: string;
  private domain: string;
  private mode: string;
  private path: string;
  private streams: any;
  private level: LogLevel;
  private rotation: any;
  private transports: any;
  private serializers: Serializers;

  constructor(options) {
    options = options || {};

    this.env = options.env || "development";
    this.domain = options.domain || "tradeOff";
    this.transports = options.transports || ["stdout"];
    this.level = process.env.LEVEL || options.level || "info";
    this.mode = process.env.MODE || options.mode || "short";
    this.path = options.path || process.cwd();

    // ensure we have a trailing slash
    if (!this.path.match(/\/$|\\$/)) {
      this.path = this.path + "/";
    }

    this.rotation = options.rotation || {
      enabled: false,
      period: "1w",
      count: 100
    };

    this.streams = {};
    this.setSerializers();

    // streams are initialized via transport
    _.each(this.transports, transport => {
      this["set" + transport.slice(0, 1).toUpperCase() + transport.slice(1) + "Stream"]();
    });
  }

  setStdoutStream = () => {
    const prettyStdOut = new PrettyStream({
      mode: this.mode
    });

    prettyStdOut.pipe(process.stdout);

    this.streams.stdout = {
      name: "stdout",
      log: bunyan.createLogger({
        name: "Log",
        streams: [
          {
            type: "raw",
            stream: prettyStdOut as any,
            level: this.level
          }
        ],
        serializers: this.serializers
      })
    };
  };

  /**
   * by default we log into two files
   * 1. file-errors: all errors only
   * 2. file-all: everything
   */
  setFileStream = () => {
    // e.g. http://my-domain.com --> http___my_domain_com
    const sanitizedDomain = this.domain.replace(/[^\w]/gi, "_");

    let loggerOptions = {
      src: false,
      serializers: this.serializers
    };

    let rotationIsEnabled = this.rotation && this.rotation.enabled;

    if (rotationIsEnabled) {
      this.streams["rotation-errors"] = {
        name: "rotation-errors",
        log: bunyan.createLogger({
          ...loggerOptions,
          name: "Log",
          streams: [
            {
              type: "rotating-file",
              path: this.path + sanitizedDomain + "_" + this.env + ".error.log",
              period: this.rotation.period,
              count: this.rotation.count,
              level: "error"
            }
          ]
        })
      };

      this.streams["rotation-all"] = {
        name: "rotation-all",
        log: bunyan.createLogger({
          ...loggerOptions,
          name: "Log",
          streams: [
            {
              type: "rotating-file",
              path: this.path + sanitizedDomain + "_" + this.env + ".log",
              period: this.rotation.period,
              count: this.rotation.count,
              level: this.level
            }
          ]
        })
      };
      return;
    }

    // error only logger
    this.streams["file-errors"] = {
      name: "file",
      log: bunyan.createLogger({
        ...loggerOptions,
        name: "Log",
        streams: [
          {
            path: this.path + sanitizedDomain + "_" + this.env + ".error.log",
            level: "error"
          }
        ]
      })
    };

    // logs everything at current log level
    this.streams["file-all"] = {
      name: "file",
      log: bunyan.createLogger({
        ...loggerOptions,
        name: "Log",
        streams: [
          {
            path: this.path + sanitizedDomain + "_" + this.env + ".log",
            level: this.level
          }
        ]
      })
    };
  };

  // @TODO: add correlation identifier
  // @TODO: res.on('finish') has no access to the response body
  setSerializers() {
    this.serializers = {
      req: req => {
        return {
          meta: {
            requestId: req.requestId,
            userId: req.userId
          },
          url: req.url,
          method: req.method,
          originalUrl: req.originalUrl,
          params: req.params,
          headers: this.removeSensitiveData(req.headers),
          body: this.removeSensitiveData(req.body),
          query: this.removeSensitiveData(req.query)
        };
      },
      res: res => {
        return {
          _headers: this.removeSensitiveData(res._headers),
          statusCode: res.statusCode,
          responseTime: res.responseTime
        };
      },
      err: err => {
        return {
          id: err.id,
          domain: this.domain,
          code: err.code,
          name: err.errorType,
          statusCode: err.statusCode,
          level: err.level,
          message: err.message,
          context: err.context,
          help: err.help,
          stack: err.stack,
          hideStack: err.hideStack,
          errorDetails: err.errorDetails
        };
      }
    };
  }

  removeSensitiveData: any = obj => {
    const newObj = {};

    _.each(obj, (value, key) => {
      try {
        if (_.isObject(value)) {
          value = this.removeSensitiveData(value);
        }

        if (!/pin|password|authorization|cookie/gi.test(key)) {
          newObj[key] = value;
        }
      } catch (err) {
        newObj[key] = value;
      }
    });

    return newObj;
  };

  /**
   * Because arguments can contain lot's of different things, we prepare the arguments here.
   * This function allows us to use logging very flexible!
   *
   * logging.info('HEY', 'DU') --> is one string
   * logging.info({}, {}) --> is one object
   * logging.error(new Error()) --> is {err: new Error()}
   */
  private log = (type, args) => {
    let modifiedArguments;

    _.each(args, value => {
      if (value instanceof Error) {
        if (!modifiedArguments) {
          modifiedArguments = {};
        }

        modifiedArguments.err = value;
      } else if (_.isObject(value)) {
        if (!modifiedArguments) {
          modifiedArguments = {};
        }

        const keys = Object.keys(value);
        _.each(keys, key => {
          modifiedArguments[key] = value[key];
        });
      } else {
        if (!modifiedArguments) {
          modifiedArguments = "";
        }

        modifiedArguments += value;
        modifiedArguments += " ";
      }
    });

    _.each(this.streams, logger => {
      // only stream the log if regex matches log entry
      // use jsonStringifySafe because req/res can contain circular dependencies
      if (logger.match) {
        if (new RegExp(logger.match).test(jsonStringifySafe(modifiedArguments).replace(/"/g, ""))) {
          logger.log[type](modifiedArguments);
        }
      } else {
        logger.log[type](modifiedArguments);
      }
    });
  };

  debug(...args) {
    this.log("debug", args);
  }

  info(...args) {
    this.log("info", args);
  }

  warn(...args) {
    this.log("warn", args);
  }

  error(...args) {
    this.log(`error`, args);
  }
}
