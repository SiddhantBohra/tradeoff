import * as _ from "lodash";
import { format } from "util";
import * as moment from "moment";
import { Transform } from "stream";
import * as prettyjson from "prettyjson";

const config = {
  levelFromName: {
    10: "trace",
    20: "debug",
    30: "info",
    40: "warn",
    50: "error",
    60: "fatal"
  },

  colorForLevel: {
    10: "grey",
    20: "grey",
    30: "cyan",
    40: "magenta",
    50: "red",
    60: "inverse"
  },

  colors: {
    bold: [1, 22],
    italic: [3, 23],
    underline: [4, 24],
    inverse: [7, 27],
    white: [37, 39],
    grey: [90, 39],
    black: [30, 39],
    blue: [34, 39],
    cyan: [36, 39],
    green: [32, 39],
    magenta: [35, 39],
    red: [31, 39],
    yellow: [33, 39]
  }
};

function colorize(color, value) {
  return "\x1B[" + config.colors[color][0] + "m" + value + "\x1B[" + config.colors[color][1] + "m";
}

export class PrettyStream extends Transform {
  private readonly mode: string;

  constructor(options: any = {}) {
    super(options);

    this.mode = options.mode || `short`;
  }

  write(chunk: any, encoding?: string | ((error: Error | null | undefined) => void), cb?: (error: Error | null | undefined) => void): boolean {
    // Bunyan sometimes passes things as objects. Because of this, we need to make sure
    // the data is converted to JSON
    if (_.isObject(chunk) && !(chunk instanceof Buffer)) {
      chunk = JSON.stringify(chunk);
    }
    return _.isString(encoding as string) ? super.write(chunk, encoding as string, cb) : super.write(chunk, encoding as string);
  }

  _transform(chunk: any, encoding?: string, callback?: Function) {
    if (!_.isString(chunk)) {
      chunk = chunk.toString();
    }

    // Remove trailing newline if any
    chunk = chunk.replace(/\\n$/, "");

    try {
      chunk = JSON.parse(chunk);
    } catch (err) {
      callback(err);
      // If data is not JSON we don't want to continue processing as if it is
      return;
    }

    let output = "";
    let time = moment(chunk.time).format("YYYY-MM-DD hh:mm:ss ");
    let logLevel = config.levelFromName[chunk.level].toUpperCase();
    let codes = config.colors[config.colorForLevel[chunk.level]];
    let bodyPretty = "";

    logLevel = "\x1B[" + codes[0] + "m" + logLevel + "\x1B[" + codes[1] + "m";

    // CASE: bunyan passes each plain string/integer as `msg` attribute (logging.info('Hey!'))
    // CASE: bunyan extended this by figuring out a message in an error object (new Error('message'))
    if (chunk.msg && !chunk.err) {
      bodyPretty += chunk.msg;

      output += format("[%s] %s %s\n", time, logLevel, bodyPretty);
    }
    // CASE: log objects in pretty JSON format
    else {
      // common log format:
      // 127.0.0.1 user-identifier user-id [10/Oct/2000:13:55:36 -0700] "GET /apache_pb.gif HTTP/1.0" 200 2326

      // if all values are available we log in common format
      // can be extended to define from outside, but not important
      try {
        output += format(
          '%s [%s] "%s %s" %s %s\n',
          logLevel,
          time,
          chunk.req.method.toUpperCase(),
          chunk.req.originalUrl,
          chunk.res.statusCode,
          chunk.res.responseTime
        );
      } catch (err) {
        output += format("[%s] %s\n", time, logLevel);
      }

      _.each(_.omit(chunk, ["time", "level", "name", "hostname", "pid", "v", "msg"]), (value: any, key: string) => {
        // we always output errors for now
        if (_.isObject(value) && value.message && value.stack) {
          let error = "\n";

          if (value.name) {
            error += colorize(config.colorForLevel[chunk.level], "NAME: " + value.name) + "\n";
          }

          if (value.code) {
            error += colorize(config.colorForLevel[chunk.level], "CODE: " + value.code) + "\n";
          }

          error += colorize(config.colorForLevel[chunk.level], "MESSAGE: " + value.message) + "\n\n";

          if (value.level) {
            error += colorize("white", "level:") + colorize("white", value.level) + "\n\n";
          }

          if (value.context) {
            error += colorize("white", value.context) + "\n";
          }

          if (value.help) {
            error += colorize("yellow", value.help) + "\n";
          }

          if (value.errorDetails) {
            error +=
              colorize(
                config.colorForLevel[chunk.level],
                "ERROR DETAILS:\n" +
                  prettyjson.render(
                    _.isArray(value.errorDetails) ? value.errorDetails[0] : value.errorDetails,
                    {
                      noColor: true
                    },
                    4
                  )
              ) + "\n\n";
          }

          if (value.stack && !value.hideStack) {
            error += colorize("white", value.stack) + "\n";
          }

          output += format("%s\n", colorize(config.colorForLevel[chunk.level], error));
        } else if (_.isObject(value)) {
          bodyPretty += "\n" + colorize("yellow", key.toUpperCase()) + "\n";

          const sanitized = {};

          _.each(value, function(innerValue, innerKey) {
            if (!_.isEmpty(innerValue)) {
              sanitized[innerKey] = innerValue;
            }
          });

          bodyPretty += prettyjson.render(sanitized, {}) + "\n";
        } else {
          bodyPretty += prettyjson.render(value, {}) + "\n";
        }
      });

      if (this.mode !== "short") {
        output += format("%s\n", colorize("grey", bodyPretty));
      }
    }

    callback(null, output);
  }
}
