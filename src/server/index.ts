// Module dependencies
import { collectDefaultMetrics } from "prom-client";
import api from "./api";
import config from "./config";
// import * as crons from "./crons";
import { HttpServer } from "./server";
import { MongoConnector } from "./models";
// initialize the server init hooks
import "./server-hooks";
import logger from "./logging";

const debug = require("debug")("TradeOff:init");

/**
 * Start monitoring this service
 * @return {Promise<void>}
 */
const initializePromClient = async () => {
  // Probe every 5th second.
  collectDefaultMetrics({ timeout: 5000 });
};

// ## Initialise Server
// Sets up the express server instances, runs init on a bunch of stuff, configures views, helpers, routes and more
// Finally it returns an instance of mainServer
const init = async () => {
  // ### Initialisation

  // Initialise the models
  await Promise.all([
    MongoConnector.connectToMongo()
    // AMQUtils.initialize()
  ]);

  // initialize AMQ workers/consumers
  // await initializeAllWorkers();

  debug(`database initialized ...`);

  // run the migrations
  /*await DistributedJobs.runOnce({key: `migrations`}, () => {
    return MongoConnector.runMigrations();
  });*/

  await Promise.all([
    api.initialize()
    // crons.scheduleCronJobs()
  ]);

  // Setup our collection of express apps
  const parentApp = await require("./app")();
  debug(`Express Apps done`);

  // const response = await auth.initialize(config);
  // parentApp.use(response.auth);
  debug(`Auth done`);

  const severInstance = new HttpServer(parentApp);
  debug(`Starting Server...`);

  // boot the server instance
  await severInstance.start(parentApp);
};

init().catch(err => {
  logger.error(err);
  process.exit(2);
});
