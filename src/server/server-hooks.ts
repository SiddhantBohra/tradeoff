import { Server } from "http";
import { Application } from "express";
import { GlobalEventRegistry } from "./events";

import { MongoConnector } from "./models";

GlobalEventRegistry.on("server.started", async (httpServer: Server, rootApp: Application) => {});

GlobalEventRegistry.on("server.shutdown", async () => {
  await Promise.all([
    // Clean state for all socket connections
    // Close all database connections
    MongoConnector.closeConnections()
  ]);
});
