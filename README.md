## Project

This is the base project to create any new application.

## Steps To follow

1. Clone the repo.
2. Edit the base project according to your request.
3. Change the remote git repo.
4. Push to the new repo.