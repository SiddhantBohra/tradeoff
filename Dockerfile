FROM node:12-slim

# define build arguments
ARG SSH_PUBLIC_KEY
ARG SSH_PRIVATE_KEY

ENV NODE_ENV=production \
    PROJECT_HOME=/usr/app/ \
    BUILD_DEPS="git python openssh-server build-essential"

# create project home
RUN mkdir -p ${PROJECT_HOME}

# switch to working directory
WORKDIR ${PROJECT_HOME}

# configure SSH
RUN mkdir -p /root/.ssh \
    && touch /root/.ssh/id_rsa \
    && touch /root/.ssh/id_rsa.pub

COPY ssh/config /root/.ssh/config

# copy SSH keys
RUN echo "$SSH_PRIVATE_KEY" >> /root/.ssh/id_rsa \
    && echo "$SSH_PUBLIC_KEY" >> /root/.ssh/id_rsa.pub

# fix the permissions for SSH private key
RUN chmod 600 /root/.ssh/id_rsa \
    && chmod 600 /root/.ssh/config

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ${PROJECT_HOME}

# install deps
RUN apt-get update > /dev/null \
    && apt-get install -y -qq --no-install-recommends ffmpeg ${BUILD_DEPS} > /dev/null \
    && npm i -g npm \
    && npm i -g --quiet pm2 swagger-merger\
    && npm install --quiet \
    && apt-get purge -y ${BUILD_DEPS} > /dev/null \
    && rm -rf /var/lib/apt/lists/*

# copy source code and install deps
COPY . $PROJECT_HOME

RUN npm run build

EXPOSE 80 443 8080

# start the application
CMD ["pm2-runtime","process.yml"]
